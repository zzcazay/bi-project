import {genChartByAiAsyncMqUsingPost, genChartByAiAsyncUsingPost} from '@/services/backend/chartController';
import { UploadOutlined } from '@ant-design/icons';
import { ProForm } from '@ant-design/pro-form';
import { Button, Card, Col, Form, message, Row, Select, Space, Upload } from 'antd';
import TextArea from 'antd/es/input/TextArea';
import React from 'react';
import useForm = ProForm.useForm;

/**
 * 添加图表页面
 * @constructor
 */
const AddChart: React.FC = () => {
  const [form] = useForm();
  const onFinish = async (values: any) => {
    const params = {
      ...values,
      file: undefined,
    };
    try {
      const res = await genChartByAiAsyncMqUsingPost(params, {}, values.file.file.originFileObj);
      if (!res?.data) {
        message.error('分析失败');
      } else {
        message.success('分析任务提交成功，稍后请在我的图表页面查看');
        form.resetFields();
      }
    } catch (e: any) {
      message.error('分析失败,' + e.message);
    }
  };

  return (
    <div className="add-chart">
      <Row gutter={24}>
        <Col span={16}>
          <Card title="图表分析" bordered={false}>
            <Form name="addChart" onFinish={onFinish} initialValues={{}} form={form}>
              <Form.Item
                name="goal"
                label="分析目标"
                labelAlign={'left'}
                rules={[{ required: true, message: '请输入分析目标!' }]}
              >
                <TextArea placeholder="请输入您的分析需求，比如：分析网站用户的增长情况" />
              </Form.Item>

              <Form.Item
                name="chartType"
                label="图表类型"
                hasFeedback
                rules={[{ required: true, message: '请输入图表类型!' }]}
              >
                <Select
                  options={[
                    { value: '折线图', label: '折线图' },
                    { value: '柱状图', label: '柱状图' },
                  ]}
                ></Select>
              </Form.Item>
              <Form.Item name="chartName" label="图表名称">
                <TextArea placeholder="请输入您的图表名称" />
              </Form.Item>

              <Form.Item name="file" label="原始数据">
                <Upload name="file" maxCount={1}>
                  <Button icon={<UploadOutlined />}>上传CSV文件</Button>
                </Upload>
              </Form.Item>

              <Form.Item wrapperCol={{ span: 12, offset: 6 }}>
                <Space>
                  <Button type="primary" htmlType="submit">
                    智能分析
                  </Button>
                  <Button htmlType="reset">重置</Button>
                </Space>
              </Form.Item>
            </Form>
          </Card>
        </Col>
      </Row>
    </div>
  );
};
export default AddChart;
