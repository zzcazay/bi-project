package com.yilaina.springbootbackend.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yilaina.springbootbackend.model.entity.User;

/**
 * 用户数据库操作
 *
 * @author yilaina
 * 
 */
public interface UserMapper extends BaseMapper<User> {

}




