package com.yilaina.springbootbackend.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yilaina.springbootbackend.model.entity.Chart;


/**
* @author z'z'ca'z'y'q
* @description 针对表【chart(图表)】的数据库操作Mapper
* @createDate 2024-02-18 11:42:23
* @Entity generator.domain.Chart
*/
public interface ChartMapper extends BaseMapper<Chart> {

}




