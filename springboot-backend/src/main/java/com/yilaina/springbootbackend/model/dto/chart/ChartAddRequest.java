package com.yilaina.springbootbackend.model.dto.chart;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 创建请求
 *
 * @author yilaina
 * 
 */
@Data
public class ChartAddRequest implements Serializable {

    /**
     * 分析目标
     */
    private String goal;

    private String chartName;

    /**
     * 图标分析数据
     */
    private String chartData;

    /**
     * 图标类型
     */
    private String chartType;

    private Long userId;



    private static final long serialVersionUID = 1L;
}