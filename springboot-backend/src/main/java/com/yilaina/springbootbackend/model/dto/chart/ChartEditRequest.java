package com.yilaina.springbootbackend.model.dto.chart;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 编辑请求
 *
 * @author yilaina
 * 
 */
@Data
public class ChartEditRequest implements Serializable {

    /**
     * id
     */
    private Long id;

    private String chartName;

    /**
     * 分析目标
     */
    private String goal;

    /**
     * 图标分析数据
     */
    private String chartData;

    /**
     * 图标类型
     */
    private String chartType;



    private static final long serialVersionUID = 1L;
}