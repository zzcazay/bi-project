package com.yilaina.springbootbackend.model.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Bi返回结果
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BiResponse {
    private String genChart;
    private String genResult;

    private Long chartId;
}
