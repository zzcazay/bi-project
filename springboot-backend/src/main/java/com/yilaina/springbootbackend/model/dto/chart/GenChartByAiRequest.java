package com.yilaina.springbootbackend.model.dto.chart;

import lombok.Data;

import java.io.Serializable;


@Data
public class GenChartByAiRequest implements Serializable {

    private String chartName;


    private String goal;

    private String chartType;


    private static final long serialVersionUID = 1L;
}
