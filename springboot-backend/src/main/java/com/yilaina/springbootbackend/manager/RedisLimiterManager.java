package com.yilaina.springbootbackend.manager;

import com.yilaina.springbootbackend.common.ErrorCode;
import com.yilaina.springbootbackend.exception.BusinessException;
import org.redisson.api.RRateLimiter;
import org.redisson.api.RateIntervalUnit;
import org.redisson.api.RateType;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 专门提供RedisLimiter 限流基础服务
 */
@Service
public class RedisLimiterManager {
    @Resource
    private RedissonClient redissonClient;


    /**
     * 限流操作
     *
     * @param key 区分不同的限流器，比如不同的用户 id 应该分别统计
     */
    public void doRateLimit(String key) {
        // 创建一个名称key的限流器
        RRateLimiter rateLimiter = redissonClient.getRateLimiter(key);

        // 每秒访问两次
        rateLimiter.trySetRate(RateType.OVERALL, 2, 1, RateIntervalUnit.SECONDS);

        // 每当一个操作来了之后，请求一个令牌（会员与普通用户不同）
        boolean canOption = rateLimiter.tryAcquire(1);
        if(!canOption) {
            throw new BusinessException(ErrorCode.TOO_MANY_REQUEST);
        }
    }
}
