package com.yilaina.springbootbackend.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yilaina.springbootbackend.model.dto.chart.ChartQueryRequest;
import com.yilaina.springbootbackend.model.dto.post.PostQueryRequest;
import com.yilaina.springbootbackend.model.entity.Chart;
import com.yilaina.springbootbackend.model.entity.Post;


/**
* @author z'z'ca'z'y'q
* @description 针对表【chart(图表)】的数据库操作Service
* @createDate 2024-02-18 11:42:23
*/
public interface ChartService extends IService<Chart> {

    /**
     * 校验
     *
     * @param post
     * @param add
     */
    void validChart(Chart chart, boolean add);

    /**
     * 获取查询条件
     *
     * @param chartQueryRequest
     * @return
     */
    QueryWrapper<Chart> getQueryWrapper(ChartQueryRequest chartQueryRequest);

}
