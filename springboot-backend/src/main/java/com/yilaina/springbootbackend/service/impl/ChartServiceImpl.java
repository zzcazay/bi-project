package com.yilaina.springbootbackend.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yilaina.springbootbackend.common.ErrorCode;
import com.yilaina.springbootbackend.constant.CommonConstant;
import com.yilaina.springbootbackend.exception.BusinessException;
import com.yilaina.springbootbackend.mapper.ChartMapper;
import com.yilaina.springbootbackend.model.dto.chart.ChartQueryRequest;
import com.yilaina.springbootbackend.model.entity.Chart;
import com.yilaina.springbootbackend.service.ChartService;
import com.yilaina.springbootbackend.utils.SqlUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * @author z'z'ca'z'y'q
 * @description 针对表【chart(图表)】的数据库操作Service实现
 * @createDate 2024-02-18 11:42:23
 */
@Service
public class ChartServiceImpl extends ServiceImpl<ChartMapper, Chart>
        implements ChartService {


    @Override
    public void validChart(Chart chart, boolean add) {


        if (chart == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }

        String goal = chart.getGoal();
        String chartData = chart.getChartData();
        String chartType = chart.getChartType();
        String genChart = chart.getGenChart();
        String genResult = chart.getGenResult();

        // 创建时，参数不能为空

        // 有参数则校验

    }

    @Override
    public QueryWrapper<Chart> getQueryWrapper(ChartQueryRequest chartQueryRequest) {


        QueryWrapper<Chart> queryWrapper = new QueryWrapper<>();
        if (chartQueryRequest == null) {
            return queryWrapper;
        }

        String sortField = chartQueryRequest.getSortField();
        String sortOrder = chartQueryRequest.getSortOrder();
        Long id = chartQueryRequest.getId();
        Long userId = chartQueryRequest.getUserId();
        String goal = chartQueryRequest.getGoal();
        String chartType = chartQueryRequest.getChartType();
        String chartName = chartQueryRequest.getChartName();

        queryWrapper.like(StringUtils.isNotBlank(goal), "goal", goal);
        queryWrapper.like(StringUtils.isNotBlank(chartType), "chartType", chartType);
        queryWrapper.like(StringUtils.isNotBlank(chartName), "chartName", chartName);

        queryWrapper.eq(id != null && id > 0, "id", id);
        queryWrapper.eq(userId != null && userId > 0, "userId", userId);
        queryWrapper.orderBy(SqlUtils.validSortField(sortField), sortOrder.equals(CommonConstant.SORT_ORDER_ASC),
                sortField);
        return queryWrapper;
    }
}




