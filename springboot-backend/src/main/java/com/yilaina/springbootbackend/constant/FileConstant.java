package com.yilaina.springbootbackend.constant;

/**
 * 文件常量
 *
 * @author yilaina
 * 
 */
public interface FileConstant {

    /**
     * COS 访问地址
     * todo 需替换配置
     */
    String COS_HOST = "https://yupi.icu";
}
