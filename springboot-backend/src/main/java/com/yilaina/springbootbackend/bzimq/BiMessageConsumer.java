package com.yilaina.springbootbackend.bzimq;

import java.util.Date;


import com.rabbitmq.client.Channel;
import com.yilaina.springbootbackend.common.ErrorCode;
import com.yilaina.springbootbackend.exception.BusinessException;
import com.yilaina.springbootbackend.exception.ThrowUtils;
import com.yilaina.springbootbackend.manager.AiManager;
import com.yilaina.springbootbackend.model.entity.Chart;
import com.yilaina.springbootbackend.service.ChartService;
import com.yilaina.springbootbackend.utils.ExcelUtils;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
@Slf4j
public class BiMessageConsumer {

    @Resource
    private ChartService chartService;

    @Resource
    private AiManager aiManager;


    @SneakyThrows
    @RabbitListener(queues = {BiMqConstant.BI_QUEUE_NAME}, ackMode = "MANUAL")
    public void receiveMessage(String message, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long deliveryTag) {

        long biModelId = 1760486543518552065L;

        if (StringUtils.isBlank(message)) {
            // 消息拒绝
            channel.basicNack(deliveryTag, false, false);
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "消息为空");
        }
        Long charId = Long.parseLong(message);
        Chart chart = chartService.getById(charId);
        if (chart == null) {
            channel.basicNack(deliveryTag, false, false);
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "图表为空");
        }

        // 修改图表状态为执行中
        Chart updateChart = new Chart();
        updateChart.setId(chart.getId());
        updateChart.setStatus("running");
        boolean b = chartService.updateById(updateChart);
        if (!b) {
            handleChartUpdateError(chart.getId(), "更新图表运行状态失败");
            return;
        }
        // 调用AI
        String result = aiManager.doChat(biModelId, buildUserInput(chart));
        String[] splits = result.split("&&&&&");
        if (splits.length < 3) {
            handleChartUpdateError(chart.getId(), "AI 生成错误");
            return;
        }
        String genChart = splits[1].trim();
        String genResult = splits[2].trim();

        // 修改图表状态为成功
        Chart updateChart2 = new Chart();
        updateChart2.setId(chart.getId());
        updateChart2.setStatus("succeed");
        updateChart2.setGenChart(genChart);
        updateChart2.setGenResult(genResult);
        boolean b2 = chartService.updateById(updateChart2);
        if (!b2) {
            handleChartUpdateError(chart.getId(), "更新图表成功状态失败");
        }
        log.info("receiveMessage message = {}", message);
        // 消息确认
        channel.basicAck(deliveryTag, false);
    }

    private String buildUserInput(Chart chart) {

        String goal = chart.getGoal();

        String chartData = chart.getChartData();
        String chartType = chart.getChartType();
        // 用户输入
        StringBuilder userInput = new StringBuilder();
        userInput.append("分析需求：").append("\n").append(goal);
        if (StringUtils.isNotBlank(chartType)) {
            userInput.append("，请使用").append(chartType);
        }
        userInput.append("\n");

        // 压缩后的数据
        userInput.append("原始数据：").append("\n").append(chartData);
        return userInput.toString();
    }

    private void handleChartUpdateError(long chartId, String execMessage) {
        Chart updateChart = new Chart();
        updateChart.setId(chartId);
        updateChart.setStatus("failed");
        updateChart.setExecMessage(execMessage);
        boolean b = chartService.updateById(updateChart);
        if (!b) {
            log.error("更新图表失败状态失败" + chartId + "," + execMessage);
        }
    }

}
