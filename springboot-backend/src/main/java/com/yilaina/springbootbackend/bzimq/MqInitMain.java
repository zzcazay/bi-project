package com.yilaina.springbootbackend.bzimq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import java.nio.charset.StandardCharsets;


/**
 * 创建测试交换机和队列
 */
public class MqInitMain {

    public static void main(String[] args) {

        try {
            // 创建连接
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("localhost");
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();

            String EXCHANGE_NAME = "code_exchange";
            String QUEUE_NAME = "code_queue";
            channel.exchangeDeclare(EXCHANGE_NAME, "direct");
            // 创建队列
            channel.queueDeclare(QUEUE_NAME, true, false, false, null);
            channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, "my_routingKey");

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
